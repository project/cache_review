<?php

namespace Drupal\Tests\cache_review\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the cache_review module.
 *
 * @group cache_review
 */
class CacheReviewTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['cache_review'];

  /**
   * The installation profile to use with this test.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * Test menu links and routes.
   */
  public function testCacheReviewPages() {
    $session = $this->assertSession();

    $account = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($account);

    $this->drupalGet('<front>');
    $session->elementExists('css', 'div.cache-status-wrapper details summary.cache-status-label');
    $session->pageTextContains('Page Cache Status |');

    // Verify cache_review pages.
    $this->drupalGet('cache-review');
    $session->linkExists('Cached page (DPC) with max-age for items');
    $session->linkExists('Uncacheable page (DPC), has lazy, some items cached due to keys');
    $session->linkExists('Cached page (DPC) with lazy items');
    $session->linkExists('Cache Review configuration');

    $this->drupalGet('cache-review/cached-max-age');
    $session->pageTextContains('Cache options: {"max-age":-1,"tags":["user:1"]}');
    $session->pageTextContains('Cache options: {"max-age":20}');
    $session->pageTextContains('Cache options: {"max-age":30,"keys":["demo_child"]}');
    $session->pageTextContains('Cache options: {"contexts":["url.query_args"]}');

    $this->drupalGet('cache-review/uncacheable-lazy-keys');
    $session->pageTextContains('The cache options for block: {"contexts":["url"]}');
    $session->pageTextContains('Cache options: {"max-age":-1}');
    $session->pageTextContains('Cache options: {"contexts":["user","url.query_args"],"keys":["demo_contexts_key"]}');
    $session->pageTextContains('Cache options: {"contexts":["url.query_args:test"],"keys":["demo_contexts_key_test"]}');
    $session->pageTextContains('This item has #lazy_builder and context session');
    $session->pageTextContains('This item has #lazy_builder and #create_placeholder');

    $this->drupalGet('cache-review/cached-lazy');
    $session->assertEscaped('Block from Lazy Builder');
    $session->pageTextContains('Cache options for block lazy: {"contexts":["user"]}');
    $session->pageTextContains('Cache options: {"max-age":-1,"keys":["keys_permanent_test"]}');
    $session->pageTextContains('Cache options: ---');
    $session->pageTextContains('Cache options: {"contexts":["url.query_args"]}');
    $session->pageTextContains('Cache options inside lazy placeholder: {"contexts":["url"]}');

  }

}
