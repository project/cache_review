<?php

namespace Drupal\Tests\cache_review\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests configuration settings.
 *
 * @group cache_review
 */
class AdminTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * Tests configuration sections.
   */
  public function testAdmin() {
    $session = $this->assertSession();
    $this->drupalGet('user/login');
    $session->pageTextNotContains('Page Cache Status |');
    $session->fieldExists('name');
    $session->fieldExists('pass');
    $admin = $this->drupalCreateUser([
      'administer site configuration',
      'administer modules',
    ]);
    $this->drupalLogin($admin);
    $this->drupalGet('admin/modules');
    $session->pageTextContains('Cache review');
    $session->pageTextContains('Provides tool to review how internal and dynamic cache work');
    $session->linkByHrefNotExists('admin/config/cache-review');
    $this->submitForm(['modules[cache_review][enable]' => 1], 'Install');
    $session->linkByHrefExists('admin/config/cache-review');
    $this->drupalGet('admin/config/cache-review');
    $this->submitForm([
      'framing' => 'no_frame',
    ], 'Save configuration');
    $this->drupalGet('<front>');
    $session->pageTextContains('Page Cache Status |');
    $session->elementExists('css', 'div.cache-status-wrapper details summary.cache-status-label');

    $this->drupalLogout();
    $this->drupalGet('user/login');
    $session->pageTextContains('Page Cache Status |');
    $session->elementExists('css', 'div.cache-status-wrapper details summary.cache-status-label');
    $session->fieldExists('name');
    $session->fieldExists('pass');
  }

}
