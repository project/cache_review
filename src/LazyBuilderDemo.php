<?php

namespace Drupal\cache_review;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class for custom lazy builder service.
 */
class LazyBuilderDemo implements TrustedCallbackInterface {

  use StringTranslationTrait;

  /**
   * Custom lazy builder callback.
   *
   * @param int $uid
   *   Current user id.
   * @param array $context_option
   *   Cache options.
   *
   * @return array
   *   Render array for lazy built item.
   */
  public function lazyBuilderCustom($uid, $context_option) {
    $time = "<span class='date'>" . date('H:i:s') . ' </span>';
    $text = sprintf('%s User id= %s. %s %s cache_review\Controller\CacheReviewController::cachedLazy', $time, $uid, $this->t('The lazy built item using service. Try to change $context_option to "user" to disable cache for element.'), $this->t('See'));
    return [
      '#markup' => sprintf('(04) %s<br><strong><i>%s %s</i></strong><hr>', $text, $this->t('Cache options inside lazy placeholder:'), json_encode(['contexts' => [$context_option]])),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'lazyBuilderCustom',
    ];
  }

}
