<?php

namespace Drupal\cache_review\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The Cache-review settings form class.
 *
 * @package Drupal\cache_review\Form
 */
class CacheReviewConfigForm extends ConfigFormBase {

  public const CACHE_REVIEW_CONF = 'cache_review.conf';

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::CACHE_REVIEW_CONF,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'ca_consultation_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CACHE_REVIEW_CONF) ?? NULL;

    $form['framing'] = [
      '#type' => 'radios',
      '#title' => $this->t('Framing options'),
      '#description' => $this->t('Select cache details level on the page'),
      '#options' => [
        'no_frame' => $this->t('Disable framing (display Page Cache Status only)'),
        'frame_all' => $this->t('Frame all items'),
        'frame_lazy' => $this->t('Frame only lazy built items (L)'),
        'frame_cached' => $this->t('Frame only cached items (C)'),
      ],
      '#default_value' => $config->get('framing') ?? 'frame_all',
    ];

    $form['html_tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('You can add id attributes to analyze'),
      '#description' => $this->t("List html tag <strong>one attribute per line</strong>. Elements with these attributes will be analyzed separately."),
      '#rows' => 6,
      '#default_value' => $config->get('html_tags'),
      '#states' => [
        'visible' => [
          ':input[name="framing"]' => [
            'value' => 'no_frame',
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['check_admin_routes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check cache information on admin pages.'),
      '#default_value' => $config->get('check_admin_routes') ?? FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config */
    $config = $this->config(self::CACHE_REVIEW_CONF);
    $values = $form_state->getValues();
    if ($values['framing'] === 'no_frame') {
      $html_tags = HTML::escape(str_replace(' ', '', $values['html_tags']));
      $config->set('html_tags', $html_tags);
    }

    $config->set('framing', $values['framing'])
      ->set('check_admin_routes', $values['check_admin_routes'])
      ->save();
    Cache::invalidateTags(['config:' . self::CACHE_REVIEW_CONF]);

    parent::submitForm($form, $form_state);
  }

}
