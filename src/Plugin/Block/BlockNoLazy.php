<?php

namespace Drupal\cache_review\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a test block.
 *
 * @Block(
 *   id = "cache_review_block_no_lazy",
 *   admin_label = @Translation("Test block without Lazy Builder"),
 *   category = @Translation("cache_review")
 * )
 */
class BlockNoLazy extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Construct BlockNoLazy object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $configuration = $this->getConfiguration();
    // With 'url' context by default.
    $context = $configuration['context_option'] ?? 'url';
    $user_name = $this->currentUser->getDisplayName();
    $text = sprintf('(00) <span class="date">%s </span><strong>Block without Lazy </strong><br> Hello <i>%s</i>,
      lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec ante auctor. In id dapibus tortor.
  Vestibulum leo quam, facilisis non porttitor et, dapibus ut elit. Suspendisse non ligula malesuada,
   imperdiet ante sagittis, lobortis odio. Nulla lacus quam, tristique non fringilla et, suscipit sit amet nisl.',
      date('H:i:s'), $user_name);

    $build['#cache'] = [
      'contexts' => [$context],
    ];
    $build['#markup'] = sprintf('%s<br><strong><i>The cache options for block: %s</i></strong><br><hr>', $text, json_encode($build['#cache']));

    return $build;
  }

}
