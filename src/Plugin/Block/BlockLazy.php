<?php

namespace Drupal\cache_review\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides a test block with Lazy Builder.
 *
 * @Block(
 *   id = "cache_review_block_lazy",
 *   admin_label = @Translation("Test block with Lazy Builder"),
 *   category = @Translation("cache_review")
 * )
 */
class BlockLazy extends BlockBase implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $configuration = $this->getConfiguration();
    $context = $configuration['context_option'] ?? 'user';

    return [
      '#lazy_builder' => [static::class . '::lazyBuilderBlock', [$context]],
      // We can force create placeholders by
      // '#create_placeholder' => TRUE.
      // Or use one of the options that is set in
      // auto_placeholdering_conditions.
      '#cache' => [
        'contexts' => [$context],
      ],
    ];
  }

  /**
   * Lazy builder callback.
   *
   * @param string $context
   *   Cache context parameter.
   *
   * @return array
   *   Render array for block.
   */
  public static function lazyBuilderBlock($context) {
    $user_name = \Drupal::currentUser()->getDisplayName();
    $text = sprintf('(00) <span class="date">%s </span><strong>Block from Lazy Builder</strong><br> Hello <i>%s</i>,
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec ante auctor,
      feugiat sem nec,mollis mi. In id dapibus tortor.Vestibulum leo quam, facilisis non porttitor et,
      dapibus ut elit. Suspendisse non ligula malesuada, imperdiet ante sagittis, lobortis odio.',
      date('H:i:s'), $user_name);

    $cache_options = [
      'contexts' => [$context],
    ];
    $output['#markup'] = sprintf('%s<br><strong><i>Cache options for block lazy: %s</i></strong><br><hr>', $text, json_encode($cache_options));
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    $callbacks[] = 'lazyBuilderBlock';
    return $callbacks;
  }

}
