// Positioning helper.
function positionTooltip() {
  // Get tooltip text sibling
  const tooltip = this.parentNode.querySelector('.cache_tooltip_text');

  // Get calculated cTooltip coordinates and size
  const cTooltipRect = this.getBoundingClientRect();

  let tipX = cTooltipRect.width + 5;
  let tipY = -40;

  tooltip.style.top = `${tipY}px`;
  tooltip.style.left = `${tipX}px`;

  // Get calculated cTooltip coordinates and size
  const tooltipRect = tooltip.getBoundingClientRect();
  // Corrections if out of window
  if (tooltipRect.x + tooltipRect.width > window.innerWidth) {
    tipX = -tooltipRect.width - 5;
  }
  if (tooltipRect.y < 0) {
    tipY -= tooltipRect.y;
  }
  tooltip.style.top = `${tipY}px`;
  tooltip.style.left = `${tipX}px`;
}

// Tooltips positioning.
const cTooltips = document.querySelectorAll('.cache_tooltip');
cTooltips.forEach(function (cTooltip, index) {
  cTooltip.addEventListener('mouseover', positionTooltip);
});
