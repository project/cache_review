CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Additional about cache
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Cache Review provides tools to help review and analyze how the internal
 (IPC) and dynamic page cache (DPC) works.
The module also provide several demo pages with cached and non-cached items.
The main goal of Cache Review project is to help developers to learn how
 cache works for anonymous, authenticated users. It should help to review
 and found possible problems with caching in projects.

Inspired by https://www.drupal.org/project/renderviz and the article
 https://weknowinc.com/blog/drupal-8-add-cache-metadata-render-arrays.

If you need to get detailed information about
cache metadata of each element on the page, please use console.

ADDITIONAL ABOUT CACHE
----------------------
 - Cache API: https://www.drupal.org/docs/8/api/cache-api/cache-api !
 - Lazy Builders: https://www.hashbangcode.com/article/drupal-9-using-lazy-builders
 - IPC: https://www.drupal.org/docs/administering-a-drupal-site/internal-page-cache
 - DPC: https://www.drupal.org/docs/8/core/modules/dynamic-page-cache/overview
 - Render arrays caching: https://www.drupal.org/docs/drupal-apis/render-api/cacheability-of-render-arrays
 - Auto-placeholdering: https://www.drupal.org/docs/drupal-apis/render-api/auto-placeholdering
 - Cache-keys: https://www.drupal.org/docs/drupal-apis/render-api/cacheability-of-render-arrays

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * This module is not for using on Production environment!
 * Install and enable this module like any other Drupal module.


CONFIGURATION
-------------
In 1.1.x version some configurations were added.
On the page /admin/config/cache-review you can set cache details level
 on the page:
- Framing options.
-- Disable framing (display Page Cache Status only).
   To show Cache page status in bottom and display full information by click.
-- Frame all items.
   To analyze all items on the page.
-- Frame only lazy built items (L).
   Only lazy built item will be framing.
-- Frame only cached items (C).
   Only separately cached item will be framing.

- Check cache information on admin pages.
  Select to analyze admin pages.

MAINTAINERS
-----------

Current maintainers:
 * https://www.drupal.org/user/3355876
